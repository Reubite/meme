Synopsis on Code for the Program 
10/11/2016

NOTES
----------------------------------------------------------
Developer: Reuben Ajayi
Course: CSC 690 (Android Development)
Program: Meme Generator Application
Primary Responsibility: Generator a meme based on user's input.

--------------------------------------------------------------------
Email: reubenajayyilsus@gmail.com
Phone: 6788133313
--------------------------------------------------------------------

www.stackoverflow.com was utilized in the development of this program and the android documentation (API Guides) was referenced while writing the code for this program. MVC pattern was utilized in the development of the meme creator program. The employed code structure was used just due to the ease of the MVC pattern and to ensure the ease of understanding the code. 
An ArrayList was rather used to hold every meme image in the project. This method seemed to be easier than the other methods to hold the memes. 

Copyright 2016. Reuben Ajayi