package com.memecreator.reubenajayi.memecreator;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by r09003799 on 10/9/2016.
 */
public class ViewNewMemeActivity extends AppCompatActivity{

    private EditText editText;
    private EditText editText2;
    private File file, file2;
    Bitmap bmp; // defined Bitmap
    Bitmap bmp2;
    ImageView imgShowOuput;
    String path;
    String path2;
    protected ImageView test;
    BitmapDrawable drawable;
    public Bitmap bitmap;
    float scale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upper_lower_input);
        test = (ImageView) findViewById(R.id.urlImage);

        file = new File(Environment.getExternalStorageDirectory() + File.separator + "saveimage");
        file.mkdirs();
        editText = (EditText) findViewById(R.id.upper_edit);
        editText2 = (EditText) findViewById(R.id.lower_edit);
        Intent intent = getIntent();
        String url_string = intent.getStringExtra("URL");
        Picasso.with(this)
                .load(url_string)
                .into(test);

        //-------------------------------------------------------------------------------------
        //--- Action (to display made meme) for button when pressed.
        final Button viewPicButton = (Button) findViewById(R.id.viewPicButton); //Display Button
        viewPicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bmp = writeUpperTextOnImage(ViewNewMemeActivity.this,
                        editText.getText().toString());
                bmp2 = writeLowerTextOnImage(ViewNewMemeActivity.this,
                        editText2.getText().toString());

                path = file.getAbsolutePath() + File.separator + System.currentTimeMillis()
                        + "_drawImage" + ".jpg";
                path2 = file.getAbsolutePath() + File.separator + System.currentTimeMillis()
                        + "_drawImage2" + ".jpg";

                saveBitmapToFile(bmp, path, 100);
                test.setImageBitmap(bmp);
                editText.setText("");
                editText2.setText("");

                Toast.makeText(ViewNewMemeActivity.this, "Text Pasted!",
                        Toast.LENGTH_SHORT).show();
                Toast.makeText(ViewNewMemeActivity.this, "Add your new meme to the list :)",
                        Toast.LENGTH_SHORT).show();
            }
        });

        //-------------------------------------------------------------------------------------
        //--- Action (to add generated meme to the meme list) for button when pressed.
        final Button addToListButton = (Button) findViewById(R.id.add_to_list); //Add Button
        addToListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewNewMemeActivity.this, NewListHomeActivity.class);
                intent.putExtra("BitmapImage", bitmap);
                startActivity(intent);
            }
        });
    }

    //------------------------------------------------------------------
    //--- Get Bitmap from View.
    public static Bitmap getBitmapFromView(View view) {
        view.clearFocus();
        view.setPressed(false);
        view.setFocusable(false);

        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(),
                view.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;
    }

    //------------------------------------------------------------------
    //--- Save Bitmap to file.
    public static String saveBitmapToFile(
            Bitmap bitmap, String path, int quality) {
        File imageFile = new File(path);
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, os);
            os.flush();
            os.close();

        } catch (Exception e) {
            Log.e("BitmapToTempFile", "Error writing bitmap", e);
        }
        return imageFile.getAbsolutePath();
    }

    //-------------------------------------------------------------------
    //--- Write upper text to url image.
    public Bitmap writeUpperTextOnImage(Context mContext, String mText) {
        try {
            Resources resources = mContext.getResources();
            scale = resources.getDisplayMetrics().density;

            /// Here you need to give your default image
            drawable = (BitmapDrawable) test.getDrawable();
            bitmap = drawable.getBitmap();

            android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();
            if (bitmapConfig == null) {
                bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
            }
            bitmap = bitmap.copy(bitmapConfig, true);
            Canvas canvas = new Canvas(bitmap);
            TextPaint paint = new TextPaint(Paint.ANTI_ALIAS_FLAG);

            // Change Text Size & Color based on your requirement
            paint.setColor(Color.WHITE);
            paint.setTextSize((int) (12 * scale));
            paint.setTextAlign(Paint.Align.LEFT);
            paint.setShadowLayer(20f, 0, 0, Color.LTGRAY);
            Rect bounds = new Rect();
            paint.getTextBounds(mText, 0, mText.length(), bounds);

            // set text width to canvas width minus 16dp padding
            int textWidth = canvas.getWidth() - (int) (16 * scale);

            // get position of text's top left corner
            float x = -bounds.left;
            float y = -bounds.top;
            // init StaticLayout for text
            StaticLayout textLayout = new StaticLayout(
                    mText, paint, textWidth, Layout.Alignment.ALIGN_CENTER, 1.0f, 1.0f, false
            );

            // get height of multiline text
            int textHeight = textLayout.getHeight();

            // draw text to the Canvas center
            canvas.save();
            canvas.translate(x, y);
            textLayout.draw(canvas);
            canvas.restore();
            ImageView myMeme = (ImageView) findViewById(R.id.urlImage);
            myMeme.setImageBitmap(bitmap);
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }

    //---------------------------------------------------------------------
    //--- Write lower text on url image.
    public Bitmap writeLowerTextOnImage(Context mContext2, String mText2) {
        try {

            Resources resources2 = mContext2.getResources();
            Canvas canvas = new Canvas(bitmap);
            TextPaint paint2 = new TextPaint(Paint.ANTI_ALIAS_FLAG);

            // Change Text Size & Color based on your requirement
            paint2.setColor(Color.WHITE);
            paint2.setTextSize((int) (12 * scale));
            paint2.setTextAlign(Paint.Align.LEFT);
            paint2.setShadowLayer(20f, 0, 0, Color.LTGRAY);
            Rect bounds = new Rect();
            paint2.getTextBounds(mText2, 0, mText2.length(), bounds);

            // set text width to canvas width minus 16dp padding
            int textWidth = canvas.getWidth() - (int) (16 * scale);

            // get position of text's top left corner
            float x = -bounds.left;
            float y = 0.80f * bitmap.getHeight();
            // init StaticLayout for text
            StaticLayout textLayout = new StaticLayout(
                    mText2, paint2, textWidth, Layout.Alignment.ALIGN_CENTER, 1.0f, 1.0f, false
            );

            // get height of multiline text
            int textHeight = textLayout.getHeight();

            // draw text to the Canvas center
            canvas.save();
            canvas.translate(x, y);
            textLayout.draw(canvas);
            canvas.restore();
            //ImageView myMeme = (ImageView) findViewById(R.id.urlImage);
            //myMeme.setImageBitmap(bitmap);
            return bitmap;

        } catch (Exception e) {
            return null;
        }
    }
}
