package com.memecreator.reubenajayi.memecreator;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class NewListHomeActivity extends AppCompatActivity {

    ListView mlist2;
    ArrayList<Bitmap> imageId = new ArrayList<Bitmap>();
    Bitmap bitmap;
    //ImageView meme_image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newlist_layout);

        Intent intent = getIntent();
        Bitmap bitmap = (Bitmap) intent.getParcelableExtra("BitmapImage");
        ImageView imageView = (ImageView)findViewById(R.id.img2);
        imageView.setImageBitmap(bitmap);
        addListenerOnButton();
    }

    public void addListenerOnButton(){
        Button backToHomeButton = (Button)findViewById(R.id.homeList_button);
        backToHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewListHomeActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}