package com.memecreator.reubenajayi.memecreator;


import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import static android.provider.Settings.Global.AIRPLANE_MODE_ON;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class URLInputActivity extends Activity {

    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_url_input);
        addListenerOnButton();
    }

    public void addListenerOnButton() {

        Button launchButton = (Button) findViewById(R.id.launch_button);
        launchButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                EditText url = (EditText) findViewById(R.id.pic_url);
                String urlText = url.getText().toString();
                loadImageFromUrl(urlText);

            }
        });
    }

    private void loadImageFromUrl(String url){
        Intent intent = new Intent(URLInputActivity.this, ViewNewMemeActivity.class);
        intent.putExtra("URL", url);
        startActivity(intent);
    }
}