package com.memecreator.reubenajayi.memecreator; /**
 * Created by r09003799 on 10/8/2016.
 */
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class Listview2 extends ArrayAdapter<String> {

    private final Activity context;
    public ArrayList<Bitmap> imageId = new ArrayList<Bitmap>();

    public Listview2(Activity context, ArrayList<Bitmap> imageId) {
        super(context, R.layout.newlist_layout);
        this.context = context;
        this.imageId = imageId;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.newlist_layout, null, true);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.img2);
        Button button = (Button) rowView.findViewById(R.id.view_button2);
        imageView.setImageBitmap(imageId.get(position));

        return rowView;
    }
}