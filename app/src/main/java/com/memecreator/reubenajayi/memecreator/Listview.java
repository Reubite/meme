package com.memecreator.reubenajayi.memecreator; /**
 * Created by r09003799 on 10/8/2016.
 */
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class Listview extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] meme;
    public ArrayList<Integer> imageId = new ArrayList<>();

    public Listview(Activity context, String[] meme, ArrayList<Integer> imageId){
        super(context, R.layout.list_layout, meme);
        this.context = context;
        this.imageId = imageId;
        this.meme = meme;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_layout, null, true);

        TextView memeName = (TextView) rowView.findViewById(R.id.txt);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        Button button = (Button) rowView.findViewById(R.id.view_button);
        memeName.setText(meme[position]);
        imageView.setImageResource(imageId.get(position));

        return rowView;
    }

//    public void updateList(ArrayList<Integer> imageId) {
//        this.imageId = imageId;
//        notifyDataSetChanged();
//    }
}
